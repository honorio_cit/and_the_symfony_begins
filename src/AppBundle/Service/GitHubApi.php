<?php

namespace AppBundle\Service;

class GitHubApi
{

  private $api_url = 'https://api.github.com/users/';

  public function getProfile($username)
  {
    $client   = new \GuzzleHttp\Client();
    $response = $client->request('GET', $this->api_url . $username);

    $data = json_decode($response->getBody()->getContents(), true);

    return array(
      'avatar_url'  => $data['avatar_url'],
      'name'        => $data['name'],
      'login'       => $data['login'],
      'details'     => array(
        'company'     => $data['company'],
        'location'    => $data['location'],
        'joined_on'   => 'Join on ' . (new \DateTime($data['created_at']))->format('m d Y'),
      ),
      'blog'        => $data['blog'],
      'social_data' => array(
        'Public Repos' => $data['public_repos'],
        'Followers'    => $data['followers'],
        'Following'    => $data['following'],
      ),
    );

  }

  public function getRepos($username)
  {
    $client   = new \GuzzleHttp\Client();
    $response = $client->request('GET', $this->api_url . $username . '/repos');

    $data = json_decode($response->getBody()->getContents(), true);

    return array(
      'repo_count' => count($data),
      'most_stars' => array_reduce(
        $data,
        function($mostStars, $currentRepo) {
          return $currentRepo['stargazers_count'] > $mostStars ? $currentRepo['stargazers_count'] : $mostStars;
        },0),
      'repos' => $data,
    );

  }

  public function getProfileAndRepos($username)
  {
    $client   = new \GuzzleHttp\Client();

    $response = $client->request('GET', $this->api_url . $username);
    $data     = json_decode($response->getBody()->getContents(), true);

    $response   = $client->request('GET', $data['repos_url']);
    $data_repos = json_decode($response->getBody()->getContents(), true);

    return array(
      'data'  => $data,
      'repos' => $data_repos,
    );
  }

}
