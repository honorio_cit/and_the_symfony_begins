<?php

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GitHutController extends Controller
{

  /**
   * @Route("/{username}",
   *   name="githut",
   *   defaults={"username" : "codereviewvideos"}
   * )
   */ 
  public function gitHutAction(Request $request, $username)
  {
    return $this->render('githut/index.html.twig', array(
      'username' => $username)
    );
  }

  /**
   * @Route("/profile/{username}",
   *   name="profile",
   *   defaults={"username" : "codereviewvideos"}
   * )
   */
  public function profileAction(Request $request, $username)
  {
    $profileData = $this->get('github_api')->getProfile($username);
    return $this->render('githut/profile.html.twig', $profileData);
  }

  /**
   * @Route("/repos/{username}",
   *   name="repos",
   *   defaults={"username" : "codereviewvideos"}
   * )
   */
  public function reposAction(Request $request, $username)
  {
    $reposData = $this->get('github_api')->getRepos($username);
    return $this->render('githut/repos.html.twig', $reposData);
  }
}

